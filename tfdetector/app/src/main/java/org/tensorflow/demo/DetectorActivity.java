/*
 * Copyright 2016 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.demo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.Build;
import android.os.SystemClock;
import android.util.Size;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.tensorflow.demo.OverlayView.DrawCallback;
import org.tensorflow.demo.env.BorderedText;
import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.tracking.MultiBoxTracker;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;
import java.lang.Math;


/**
 * An activity that uses a TensorFlowMultiBoxDetector and ObjectTracker to detect and then track
 * objects.
 */
public class DetectorActivity extends CameraActivity implements OnImageAvailableListener {
  private static final Logger LOGGER = new Logger();

  // Configuration values for the prepackaged multibox model.
  private static final int MB_INPUT_SIZE = 224;
  private static final int MB_IMAGE_MEAN = 128;
  private static final float MB_IMAGE_STD = 128;
  private static final String MB_INPUT_NAME = "ResizeBilinear";
  private static final String MB_OUTPUT_LOCATIONS_NAME = "output_locations/Reshape";
  private static final String MB_OUTPUT_SCORES_NAME = "output_scores/Reshape";
  private static final String MB_MODEL_FILE = "file:///android_asset/multibox_model.pb";
  private static final String MB_LOCATION_FILE =
      "file:///android_asset/multibox_location_priors.txt";

  private static final int TF_OD_API_INPUT_SIZE = 300;
  private static final String TF_OD_API_MODEL_FILE =
      "file:///android_asset/frozen_inference_graph.pb";
  private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/machine_label.txt";

  // Configuration values for tiny-yolo-voc. Note that the graph is not included with TensorFlow and
  // must be manually placed in the assets/ directory by the user.
  // Graphs and models downloaded from http://pjreddie.com/darknet/yolo/ may be converted e.g. via
  // DarkFlow (https://github.com/thtrieu/darkflow). Sample command:
  // ./flow --model cfg/tiny-yolo-voc.cfg --load bin/tiny-yolo-voc.weights --savepb --verbalise
  private static final String YOLO_MODEL_FILE = "file:///android_asset/tiny-yolo.pb";
  private static final int YOLO_INPUT_SIZE = 416;
  private static final String YOLO_INPUT_NAME = "input";
  private static final String YOLO_OUTPUT_NAMES = "output";
  private static final int YOLO_BLOCK_SIZE = 32;

  // Which detection model to use: by default uses Tensorflow Object Detection API frozen
  // checkpoints.  Optionally use legacy Multibox (trained using an older version of the API)
  // or YOLO.
  //private enum DetectorMode {
  //  TF_OD_API, MULTIBOX, YOLO;
  //}
  //private static final DetectorMode MODE = DetectorMode.TF_OD_API;


  // Minimum detection confidence to track a detection.
  public float MINIMUM_CONFIDENCE_TF_OD_API = 0.5f;
  private static final float MINIMUM_CONFIDENCE_MULTIBOX = 0.1f;
  public float MINIMUM_CONFIDENCE_YOLO = 0.5f;
  public String Mode;

  private final boolean MAINTAIN_ASPECT = Mode == "YOLO";

  private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);

  private static final boolean SAVE_PREVIEW_BITMAP = false;
  private static final float TEXT_SIZE_DIP = 10;

  private Integer sensorOrientation;

  private Classifier detector;

  private long lastProcessingTimeMs;
  private Bitmap rgbFrameBitmap = null;
  private Bitmap croppedBitmap = null;
  private Bitmap cropCopyBitmap = null;

  private boolean computingDetection = false;

  private long timestamp = 0;

  private Matrix frameToCropTransform;
  private Matrix cropToFrameTransform;

  public MultiBoxTracker tracker; //made PUBLIC from PRIVATE

  private byte[] luminanceCopy;

  private BorderedText borderedText;

  private PopupWindow mPopupWindow, sliderPopup;

  private Context mContext, sContext;
  public float thresh;
  public int width;
  public float height;

  public String selectedDetector;
  public String Yolo = "YOLO";
  public String Multibox = "MULTIBOX";
  public String Ssd = "TF_OD_API";
  //public String warnMessage = "Please tap directly on object";
  public String returnCorrespodingLabel(ArrayList<String> bboxlabel, ArrayList<ArrayList<Float>> bboxs, int tapX, int tapY){
      LOGGER.i(bboxs.toString());
      LOGGER.i(bboxlabel.toString());
      LOGGER.i("number of bboxes to consider");
      LOGGER.i(Integer.toString(bboxlabel.size()));
      LOGGER.i("number here should match number above");
      LOGGER.i(Integer.toString(bboxs.size()));

      String returnedLabel = "";
      myLabel:for(int i = 0; i < bboxlabel.size(); i++){
          LOGGER.i("i count");
          LOGGER.i(Integer.toString(i));
          int top = Math.round(bboxs.get(i).get(0));
          int left = Math.round(bboxs.get(i).get(1));
          int right = Math.round(bboxs.get(i).get(2));
          int bottom = Math.round(bboxs.get(i).get(3));
          if (left <= tapX && tapX <= right) {
              if (top <= tapY && tapY <= bottom) {
                  returnedLabel = bboxlabel.get(i);
                  LOGGER.i("match found");
                  break;
              }
              else{
                  continue myLabel;
              }
          }
          else {
              continue myLabel;
          }
      }
      LOGGER.i("returned label string");
      LOGGER.i(returnedLabel);
      return returnedLabel;
  }

  @Override
  public void onPreviewSizeChosen(final Size size, final int rotation) {
    final float textSizePx =
        TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
    borderedText = new BorderedText(textSizePx);
    borderedText.setTypeface(Typeface.MONOSPACE);

    tracker = new MultiBoxTracker(this);
    Intent detectorIntent = getIntent();
    Mode = detectorIntent.getStringExtra(ModelSelection.EXTRA_MESSAGE);
    LOGGER.i("*********************");
    LOGGER.i(Mode);
    //Mode = selectedDetector;

    int cropSize = TF_OD_API_INPUT_SIZE;
    if (Mode.equals(Yolo)) {
      detector =
          TensorFlowYoloDetector.create(
              getAssets(),
              YOLO_MODEL_FILE,
              YOLO_INPUT_SIZE,
              YOLO_INPUT_NAME,
              YOLO_OUTPUT_NAMES,
              YOLO_BLOCK_SIZE);
      cropSize = YOLO_INPUT_SIZE;
      Toast toast = Toast.makeText(getApplicationContext(), Mode, Toast.LENGTH_SHORT);
      toast.show();
    } else if (Mode.equals(Multibox)) {
      detector =
          TensorFlowMultiBoxDetector.create(
              getAssets(),
              MB_MODEL_FILE,
              MB_LOCATION_FILE,
              MB_IMAGE_MEAN,
              MB_IMAGE_STD,
              MB_INPUT_NAME,
              MB_OUTPUT_LOCATIONS_NAME,
              MB_OUTPUT_SCORES_NAME);
      cropSize = MB_INPUT_SIZE;
    } else {
      try {
        detector = TensorFlowObjectDetectionAPIModel.create(
            getAssets(), TF_OD_API_MODEL_FILE, TF_OD_API_LABELS_FILE, TF_OD_API_INPUT_SIZE);
        cropSize = TF_OD_API_INPUT_SIZE;
        Toast toast = Toast.makeText(getApplicationContext(), Mode, Toast.LENGTH_SHORT);
        toast.show();
      } catch (final IOException e) {
        LOGGER.e("Exception initializing classifier!", e);
        Toast toast =
            Toast.makeText(
                getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
        toast.show();
        finish();
      }
    }

    previewWidth = size.getWidth();
    previewHeight = size.getHeight();

    sensorOrientation = rotation - getScreenOrientation();
    LOGGER.i("Camera orientation relative to screen canvas: %d", sensorOrientation);

    LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
    rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Config.ARGB_8888);
    croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Config.ARGB_8888);

    frameToCropTransform =
        ImageUtils.getTransformationMatrix(
            previewWidth, previewHeight,
            cropSize, cropSize,
            sensorOrientation, MAINTAIN_ASPECT);

    cropToFrameTransform = new Matrix();
    frameToCropTransform.invert(cropToFrameTransform);
    trackingOverlay = (OverlayView) findViewById(R.id.tracking_overlay);

    trackingOverlay.addCallback(
            new DrawCallback() {
                @Override
                public void drawCallback(final Canvas canvas) {
                    tracker.draw(canvas);
                    if (isDebug()) {
                        tracker.drawDebug(canvas);
                    }
                }
            });

    trackingOverlay.setOnTouchListener(new View.OnTouchListener(){
          @Override
          public boolean onTouch(View v, MotionEvent event){
              mContext = getApplicationContext();
              tapLocationX = (int) event.getX();
              tapLocationY = (int) event.getY();
              String popupLabel;
              popupLabel = returnCorrespodingLabel(tracker.labels, tracker.bbox, tapLocationX, tapLocationY);
              LOGGER.i("popupLabel");
              LOGGER.i(popupLabel);
              LOGGER.i(Integer.toString((int) 0.6f*previewWidth));
              LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
              // Inflate the custom layout/view
              View popupView = inflater.inflate(R.layout.custom_layout1,null);
              // Initialize a new instance of popup window
              mPopupWindow = new PopupWindow(
                      popupView,
                      (int) (1.5f*previewWidth),
                      (int) (2.5f * previewHeight)
              );
              TextView tv = popupView.findViewById(R.id.tv);
              TextView desc = popupView.findViewById(R.id.description);
              if(Build.VERSION.SDK_INT>=21){
                  mPopupWindow.setElevation(5.0f);
              }
              String description;
              String cunit = "Control_unit";
              String fuse = "Fuse";
              String transformer = "Transformer";
              String regulator = "Regulator";
              String swtch = "Switches";
              if (popupLabel.equals(fuse)){
                  description = "A fuse is an electrical safety device that operates to provide overcurrent protection of an electrical circuit.Its essential component is a metal wire or strip that melts when too much current flows through it, thereby interrupting the current.It is a sacrificial device; once a fuse has operated it is an open circuit, and it must be replaced or rewired, depending on type.Fuses have been used as essential safety devices from the early days of electrical engineering.";
                  popupLabel = "Fuse";
              }
              else if (popupLabel.equals(cunit)){
                  description = "An Electronic Control Unit (ECU) is any embedded system in automotive electronics that controls one or more of the electrical systems or subsystems in a vehicle.Types of ECU include Engine Control Module (ECM), Powertrain Control Module (PCM),Transmission Control Module (TCM), Brake Control Module (BCM or EBCM), Central Control Module (CCM),Central Timing Module (CTM), General Electronic Module (GEM), Body Control Module (BCM),Suspension Control Module (SCM), control unit, or control module.";
                  popupLabel = "Control Unit";
              }
              else if (popupLabel.equals(swtch)){
                  description = "In electrical engineering, a switch is an electrical component that can \"make\" or \"break\" an electrical circuit, interrupting the current or diverting it from one conductor to another.The mechanism of a switch removes or restores the conducting path in a circuit when it is operated.";
                  popupLabel = "Switch";
              }
              else if (popupLabel.equals(regulator)){
                  description = "A regulator is a device which has the function of maintaining a designated characteristic.It performs the activity of managing or maintaining a range of values in a machine.";
                  popupLabel = "Regulator";
              }
              else if (popupLabel.equals(transformer)){
                  description = "A transformer is a static electrical device that transfers electrical energy between two or more circuits. ";
                  popupLabel = "Transformer";
              }
              else{
                  description = "Invalid";
              }
              desc.setText(description);
              tracker.labels.clear();
              tracker.bbox.clear();
              tv.setText(popupLabel);
              if (popupLabel != ""){
                  mPopupWindow.showAtLocation(trackingOverlay, Gravity.CENTER,0,-300);
                  ImageButton closeButton = (ImageButton) popupView.findViewById(R.id.popup_close);
                  closeButton.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View popupView) {
                          mPopupWindow.dismiss();
                      }
                  });
              }
              else{
                  String warnMessage = "Please tap on an object label";
                  Toast.makeText(mContext, warnMessage, Toast.LENGTH_SHORT).show();
              }
              return false;
        }
      });

      Button optionsButton = (Button) findViewById(R.id.optionsButton);
      final TextView currentThreshold = findViewById(R.id.currentThresh);
      optionsButton.setOnClickListener(new View.OnClickListener(){
          @Override
          public void onClick(View v){
              sContext = DetectorActivity.this;
              //LayoutInflater sliderInflater = (LayoutInflater) sContext.getSystemService(LAYOUT_INFLATER_SERVICE);
              LayoutInflater sliderInflater = getLayoutInflater();
              final View sliderView = sliderInflater.inflate(R.layout.custom_layout,null);
              // Initialize a new instance of popup window
              sliderPopup = new PopupWindow(
                      sliderView,
                      (int) (1.5f*previewWidth),
                      (int) (1.0f * previewHeight)
              );
              sliderPopup.showAtLocation(sliderView, Gravity.CENTER, 0, -500 );
              SeekBar thresholdSlider = sliderView.findViewById(R.id.seekBar);
              ImageButton closeButton = sliderView.findViewById(R.id.sliderClose);
              closeButton.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      sliderPopup.dismiss();
                  }
              });

              thresholdSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                  @Override
                  public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                      // updated continuously as the user slides the thumb
                      TextView tvProgressLabel = sliderView.findViewById(R.id.textView);
                      tvProgressLabel.setText("Threshold: " + progress);
                      //float progress = thresholdSlider.getProgress();
                      thresh = (float) progress/100;
                      currentThreshold.setText("Threshold: " + progress);
                  }
                  @Override
                  public void onStartTrackingTouch(SeekBar seekBar) {

                  }
                  @Override
                  public void onStopTrackingTouch(SeekBar seekBar) {

                  }
              });


          }
      });



    addCallback(
        new DrawCallback() {
          @Override
          public void drawCallback(final Canvas canvas) {
            if (!isDebug()) {
              return;
            }
            final Bitmap copy = cropCopyBitmap;
            if (copy == null) {
              return;
            }

            final int backgroundColor = Color.argb(100, 0, 0, 0);
            canvas.drawColor(backgroundColor);

            final Matrix matrix = new Matrix();
            final float scaleFactor = 2;
            matrix.postScale(scaleFactor, scaleFactor);
            matrix.postTranslate(
                canvas.getWidth() - copy.getWidth() * scaleFactor,
                canvas.getHeight() - copy.getHeight() * scaleFactor);
            canvas.drawBitmap(copy, matrix, new Paint());

            final Vector<String> lines = new Vector<String>();
            if (detector != null) {
              final String statString = detector.getStatString();
              final String[] statLines = statString.split("\n");
              for (final String line : statLines) {
                lines.add(line);
              }
            }
            lines.add("");

            lines.add("Frame: " + previewWidth + "x" + previewHeight);
            lines.add("Crop: " + copy.getWidth() + "x" + copy.getHeight());
            lines.add("View: " + canvas.getWidth() + "x" + canvas.getHeight());
            lines.add("Rotation: " + sensorOrientation);
            lines.add("Inference time: " + lastProcessingTimeMs + "ms");

            borderedText.drawLines(canvas, 10, canvas.getHeight() - 10, lines);
          }
        });
  }

  OverlayView trackingOverlay;

  @Override
  protected void processImage() {
    ++timestamp;
    final long currTimestamp = timestamp;
    byte[] originalLuminance = getLuminance();
    tracker.onFrame(
        previewWidth,
        previewHeight,
        getLuminanceStride(),
        sensorOrientation,
        originalLuminance,
        timestamp);
    trackingOverlay.postInvalidate();

    // No mutex needed as this method is not reentrant.
    if (computingDetection) {
      readyForNextImage();
      return;
    }
    computingDetection = true;
    LOGGER.i("Preparing image " + currTimestamp + " for detection in bg thread.");

    rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

    if (luminanceCopy == null) {
      luminanceCopy = new byte[originalLuminance.length];
    }
    System.arraycopy(originalLuminance, 0, luminanceCopy, 0, originalLuminance.length);
    readyForNextImage();

    final Canvas canvas = new Canvas(croppedBitmap);
    canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
    // For examining the actual TF input.
    if (SAVE_PREVIEW_BITMAP) {
      ImageUtils.saveBitmap(croppedBitmap);
    }

    runInBackground(
        new Runnable() {
          @Override
          public void run() {
            LOGGER.i("Running detection on image " + currTimestamp);
            final long startTime = SystemClock.uptimeMillis();
            final List<Classifier.Recognition> results = detector.recognizeImage(croppedBitmap);
            lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

            cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
            final Canvas canvas = new Canvas(cropCopyBitmap);
            final Paint paint = new Paint();
            paint.setColor(Color.RED);
            paint.setStyle(Style.STROKE);
            paint.setStrokeWidth(2.0f);

            float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
            if (Mode.equals(Ssd)){
                minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
              }
            else if (Mode.equals(Multibox)){
                minimumConfidence = MINIMUM_CONFIDENCE_MULTIBOX;
            }
            else if (Mode.equals(Yolo)){
                minimumConfidence = MINIMUM_CONFIDENCE_YOLO;
            }

            final List<Classifier.Recognition> mappedRecognitions =
                new LinkedList<Classifier.Recognition>();

            for (final Classifier.Recognition result : results) {
              final RectF location = result.getLocation();
              if (location != null && result.getConfidence() >= thresh) {
                  String intthresh = String.valueOf(thresh);
                  LOGGER.i("********************************************************************************************");
                  LOGGER.i(intthresh);
                canvas.drawRect(location, paint);

                cropToFrameTransform.mapRect(location);
                result.setLocation(location);
                mappedRecognitions.add(result);
              }
            }

            tracker.trackResults(mappedRecognitions, luminanceCopy, currTimestamp);
            //LOGGER.i(tracker)
            trackingOverlay.postInvalidate();

            requestRender();
            computingDetection = false;
          }
        });
  }

  @Override
  protected int getLayoutId() {
    return R.layout.camera_connection_fragment_tracking;
  }

  @Override
  protected Size getDesiredPreviewFrameSize() {
    return DESIRED_PREVIEW_SIZE;
  }

  @Override
  public void onSetDebug(final boolean debug) {
    detector.enableStatLogging(debug);
  }
}
