package org.tensorflow.demo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.tensorflow.demo.env.Logger;

public class ModelSelection extends Activity {

    private RadioGroup radioModelGroup;
    private RadioButton radioModelButton;
    private Button btnDetect;
    public String chosenModel;
    public static final String EXTRA_MESSAGE = "Selected_model";
    private static final Logger LOGGER = new Logger();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_selection);

        addListenerOnButton();

    }

    /*public void launchDetectorActivity(View view) {
        Intent intent = new Intent(this, DetectorActivity.class);
        //intent.putExtra(EXTRA_MESSAGE,chosenModel);
        LOGGER.i("here");
        startActivity(intent);
    }*/

    public void addListenerOnButton() {

        radioModelGroup = (RadioGroup) findViewById(R.id.radioModel);
        btnDetect = (Button) findViewById(R.id.detectButton);

        btnDetect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // get selected radio button from radioGroup
                int selectedId = radioModelGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioModelButton = (RadioButton) findViewById(selectedId);

                //launch DetectorActivity and pass model name
                Toast.makeText(ModelSelection.this,
                        radioModelButton.getText(), Toast.LENGTH_SHORT).show();
                chosenModel = radioModelButton.getText().toString();
                Intent intent = new Intent(getApplicationContext(), DetectorActivity.class);
                intent.putExtra(EXTRA_MESSAGE,chosenModel);
                LOGGER.i("here");
                startActivity(intent);

            }

        });

    }
}
